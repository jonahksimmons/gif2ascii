GIF2ASCII(1)                                  General Commands Manual                                  GIF2ASCII(1)

NAME
       gif2ascii - view .GIF files in ascii

SYNOPSIS
       gif2ascii [file]

DESCRIPTION
       converts a .GIF file into a directory of ascii files.

OPTIONS
       -h, --help
              display this help and exit

GNU                                                  2021-12-11                                        GIF2ASCII(1)
